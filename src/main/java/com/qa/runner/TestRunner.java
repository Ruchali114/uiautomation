package com.qa.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
		
		features="src/main/java/com/qa/feature",
		glue= {"stepDefinitions"},
		monochrome=true		
		)
public class TestRunner {

	
}
