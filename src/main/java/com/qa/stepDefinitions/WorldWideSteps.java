package com.qa.stepDefinitions;

import com.qa.pages.WorldWidepage;
import com.qa.util.TestBase;

import io.cucumber.java.en.Given;

public class WorldWideSteps extends TestBase{

	WorldWidepage obj =new WorldWidepage();
	
	@Given("User clicks on Worldwide")
	public void user_clicks_on_worldwide() {
	    
		obj.clickWorldWide();
	}

	@Given("User clicks on links")
	public void user_clicks_on_links() {
	    
		obj.linksDescription();
	}
}
