package com.qa.stepDefinitions;

import org.testng.Assert;

import com.qa.pages.AutomationPage;
import com.qa.util.TestBase;

import io.cucumber.java.en.Then;

public class AutomationPageSteps extends TestBase {

	AutomationPage obj = new AutomationPage();

	@Then("User fill the form successfully")
	public void user_fill_the_form_successfully() throws InterruptedException {
	   
		String expected=obj.formFill(prop.getProperty("firstName"), prop.getProperty("lastname"), 
				prop.getProperty("email"), prop.getProperty("phoneNumber"), prop.getProperty("textBox"));
		
		Assert.assertEquals(expected, "Thank you for contacting us.");
	}

}
