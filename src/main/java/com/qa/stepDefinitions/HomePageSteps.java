package com.qa.stepDefinitions;

import org.junit.Assert;

import com.qa.pages.AutomationPage;
import com.qa.pages.HomePage;
import com.qa.util.TestBase;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class HomePageSteps extends TestBase {

	HomePage homepage = new HomePage();
	AutomationPage automationLinkTitle = new AutomationPage();

	@Given("User navigate to Homepage")
	public void user_navigate_to_homepage() {

		TestBase.initialization();
	}

	@Given("User clicks on Automation under Services tab")
	public void user_clicks_on_automation_under_services_tab() {

		homepage.navigateToAutomation();

	}

	@Then("User navigate to Automation page")
	public void user_navigate_to_automation_page() {

		String expectedResult = automationLinkTitle.automationheadingText();
		Assert.assertEquals(expectedResult, "Automation");

	}

}
