package com.qa.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.qa.util.TestBase;

public class AutomationPage extends TestBase {

	@FindBy(xpath = "//div[@class='page-heading']")
	WebElement automationHeading;

	@FindBy(xpath = "//label[@class='Form__Element__Caption']")
	WebElement contactUs;

	@FindBy(xpath = "//input[@class='FormTextbox__Input']")
	WebElement firstName;

	@FindBy(xpath = "//input[@name='__field_123938']")
	WebElement lastName;

	@FindBy(xpath = "//input[@name='__field_123928']")
	WebElement email;

	@FindBy(xpath = "//input[@name='__field_123929']")
	WebElement phoneNumber;

	@FindBy(xpath = "//select[@name='__field_132596']")
	WebElement countryDD;

	@FindBy(xpath = "//textarea[@name='__field_123931']")
	WebElement textBox;

	@FindBy(xpath = "//input[@type='checkbox']")
	WebElement checkBox;

	@FindBy(xpath = "//button[@id='06838eea-8980-4305-83d0-42236fb4d528']")
	WebElement submitButton;

	@FindBy(xpath = "//div[@class='Form__Status__Message Form__Success__Message']")
	WebElement successMessage;

	public AutomationPage() {

		PageFactory.initElements(driver, this);
	}

	public String automationheadingText() {

		return automationHeading.getText();
	}

	public String formFill(String firstName, String lastName, String email, String phoneNumber, String textBox)
			throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", contactUs);

		this.firstName.sendKeys(firstName);
		this.lastName.sendKeys(lastName);
		this.email.sendKeys(email);
		this.phoneNumber.sendKeys(phoneNumber);

		Select country = new Select(this.countryDD);
		country.selectByIndex(2);

		this.textBox.sendKeys(textBox);
		this.checkBox.click();

		Thread.sleep(50000);
		this.submitButton.click();

		return this.successMessage.getText();

	}
}
