package com.qa.pages;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qa.util.TestBase;

public class WorldWidepage extends TestBase{

	@FindBy(xpath="//div[@class='sprite-header sprite-global-arrowdown']")
	WebElement worldWide;
	
	@FindBy(xpath="//*[@class='country-list']/ul/li/a")
	List<WebElement> listlinks;
	
	public void clickWorldWide() {
		
		worldWide.click();
	}
	
	public void linksDescription() {
		
		String parentWindow = driver.getWindowHandle();
		for (WebElement dropDownLink : listlinks) {
			
			String parentURL=dropDownLink.getAttribute("href");
			dropDownLink.click();
			
			Set<String> s1 = driver.getWindowHandles();
			Iterator<String> i1 = s1.iterator();
			
			while (i1.hasNext()) {
				String ChildWindow = i1.next();
				if (!parentWindow.equalsIgnoreCase(ChildWindow)) {
					driver.switchTo().window(ChildWindow);
					
					String childURL=driver.getCurrentUrl();
					
					Assert.assertEquals(childURL, parentURL);
					driver.close();
				}
				
			}

			driver.switchTo().window(parentWindow);
			
		}
	}
	
	
}
