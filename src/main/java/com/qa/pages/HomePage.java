package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.TestBase;

public class HomePage extends TestBase {

	@FindBy(xpath = "//button[@class='acceptCookie']")
	WebElement acceptCookies;

	@FindBy(xpath = "//*[@id='main-menu']/ul/li[3]/div")
	WebElement serviceslink;

	@FindBy(xpath = "//*[@id='main-menu']/ul/li[3]/div[2]/ul/li[4]")
	WebElement automationLink;

	@FindBy(xpath = "//div[@class='sprite-header sprite-global-arrowdown']")
	WebElement worldwideDD;

	public HomePage() {

		PageFactory.initElements(driver, this);
	}

	public void navigateToAutomation() {

		Actions act = new Actions(driver);
		act.moveToElement(serviceslink).perform();
		automationLink.click();
	}

}
